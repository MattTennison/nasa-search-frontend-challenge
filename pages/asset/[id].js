import { useRouter } from "next/router";
import useSWR from "swr";
import Heading from "../../components/Heading";
import { getImageInformation } from "../../services/nasa-api";
import styles from "./id.module.css";

/**
 * If I spent more time on this page, I'd add some sort of skeleton loading UI
 * Probably using this Tailwind Animation - https://tailwindcss.com/docs/animation#pulse
 */
export default function ItemView() {
  const router = useRouter();
  const { id } = router.query;

  const { data } = useSWR(id, getImageInformation);

  return data ? (
    <>
      <Heading text={data.title} />
      <p>{data.description}</p>
      <img
        src={data.asset.url}
        style={{
          aspectRatio: data.asset.aspectRatio.toString(), // not sure why I had to manually call toString() but it didn't work when it was a number
        }}
        alt={data.description}
        className={styles.image}
      />
    </>
  ) : null;
}
