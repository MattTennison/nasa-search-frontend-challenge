import Head from "next/head";
import { useState } from "react";
import Heading from "../components/Heading";
import SearchForm from "../components/SearchForm";
import ResultsList from "../components/ResultsList";

export default function Home() {
  const [shouldShowResults, setShouldShowResults] = useState(false);
  const [query, setQuery] = useState("");
  const [mediaTypes, setMediaTypes] = useState({});

  const handleSearch = ({
    query,
    options: { isImageChecked, isAudioChecked },
  }) => {
    setQuery(query);
    setMediaTypes({
      includeAudio: isAudioChecked,
      includeImages: isImageChecked,
    });
    setShouldShowResults(true);
  };

  return (
    <>
      <Head>
        <title>NASA Search</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Heading text="NASA Search" />
      <SearchForm handleSearch={handleSearch} />
      {shouldShowResults ? (
        <ResultsList query={query} mediaTypes={mediaTypes} />
      ) : null}
    </>
  );
}
