import styles from "./Page.module.css";

export default function Page({ children }) {
  return (
    <div className={styles.outer}>
      <div className={styles.inner}>{children}</div>
    </div>
  );
}
