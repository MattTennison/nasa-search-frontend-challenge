import { useState } from "react";
import styles from "./SearchForm.module.css";

export default function SearchForm({ handleSearch }) {
  const [searchQuery, setSearchQuery] = useState("");
  const [isImageChecked, setIsImageChecked] = useState(true);
  const [isAudioChecked, setIsAudioChecked] = useState(false);

  const handleFormSubmission = (e) => {
    e.preventDefault();
    handleSearch({
      query: searchQuery,
      options: { isImageChecked, isAudioChecked },
    });
  };

  return (
    <form onSubmit={handleFormSubmission} className={styles.form}>
      <input
        type="search"
        value={searchQuery}
        onChange={(e) => setSearchQuery(e.target.value)}
        className={styles.input}
      />

      <input type="submit" aria-label="Submit search query" value="🔎" />

      <div>
        <input
          type="checkbox"
          name="Images"
          value="images"
          id="image"
          checked={isImageChecked}
          onChange={(e) =>
            setIsImageChecked((isImageChecked) => !isImageChecked)
          }
          className={styles.inputLabelled}
        />
        <label htmlFor="images">Images</label>
      </div>

      <div className="col-span-2">
        <input
          type="checkbox"
          name="Audio"
          value="audio"
          id="audio"
          checked={isAudioChecked}
          onChange={(e) =>
            setIsAudioChecked((isAudioChecked) => !isAudioChecked)
          }
          disabled
        />
        <label htmlFor="audio">Audio</label>
      </div>
    </form>
  );
}
