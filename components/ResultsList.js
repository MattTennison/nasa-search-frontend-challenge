import useSWR from "swr";
import Link from "next/link";
import { search } from "../services/nasa-api";
import styles from "./ResultsList.module.css";

export default function ResultsList({ query, mediaTypes }) {
  const { data } = useSWR([query, mediaTypes], (query, mediaTypes) =>
    search(query, { mediaTypes })
  );

  return data ? (
    <div className={styles.resultsList}>
      <ul>
        {data.map((result) => (
          <li key={result.id}>
            <Link href={`/asset/${result.id}`}>
              <a href={`/asset/${result.id}`}>
                <img
                  src={result.asset.preview.src}
                  loading="lazy"
                  alt={result.description}
                />
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  ) : null;
}
