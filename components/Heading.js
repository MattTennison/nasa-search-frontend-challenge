export default function Heading({ level = 1, text }) {
  const Tag = `h${level}`;

  return <Tag>{text}</Tag>;
}
