import nock from "nock";
import nasaSearchResponse from "../fixtures/nasa-search.json";
import { search } from "./nasa-api";

describe("search", () => {
  test("returns data when the NASA API returns succesfully", async () => {
    nock("https://images-api.nasa.gov")
      .get("/search")
      .query(true)
      .reply(200, nasaSearchResponse, {
        "Access-Control-Allow-Origin": "*",
      });

    const result = await search("clown", {
      mediaTypes: { includeImages: true, includeAudio: false },
    });

    expect(result).toMatchSnapshot();
  });

  test("calls the API correctly", async () => {
    const requestMock = nock("https://images-api.nasa.gov")
      .get("/search")
      .query({ q: "clown", media_type: "audio,image" })
      .reply(200, nasaSearchResponse, {
        "Access-Control-Allow-Origin": "*",
      });

    await search("clown", {
      mediaTypes: { includeImages: true, includeAudio: true },
    });

    expect(requestMock.isDone()).toBe(true);
  });

  test("throws error if no query is supplied", () => {
    expect(
      search(null, { mediaTypes: { includeAudio: true } })
    ).rejects.toThrowError("No query provided");
  });
});

describe("getImageInformation", () => {
  test.todo("throws error if no id provided");

  test.todo("returns data when both APIs return successfully");

  test.todo("calls the APIs correctly");
});
