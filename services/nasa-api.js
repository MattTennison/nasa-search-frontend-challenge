export const getImageInformation = async (id) => {
  /**
   * If I was building this out for a real app I'd consider using TypeScript
   * instead of this run-time check
   */
  if (!id || typeof id !== "string") {
    throw new Error("Invalid ID");
  }

  const [urls, metadata] = await Promise.all([
    fetch(`https://images-api.nasa.gov/asset/${id}`).then((res) => res.json()),
    fetch(`https://images-assets.nasa.gov/image/${id}/metadata.json`).then(
      (res) => res.json()
    ),
  ]);

  return {
    asset: {
      url: urls.collection.items[0].href,
      aspectRatio: metadata["File:ImageWidth"] / metadata["File:ImageHeight"],
    },
    description: metadata["XMP:Description"],
    title: metadata["XMP:Title"],
  };
};

const getMediaTypes = (includeAudio, includeImages) => {
  const mediaTypes = [];
  if (includeAudio) {
    mediaTypes.push("audio");
  }
  if (includeImages) {
    mediaTypes.push("image");
  }
  return mediaTypes;
};

export const search = async (
  query,
  { mediaTypes: { includeAudio, includeImages } }
) => {
  if (!query || typeof query !== "string") {
    throw new Error("No query provided");
  }

  const endpoint = new URL("https://images-api.nasa.gov/search");
  endpoint.searchParams.set("q", query);
  endpoint.searchParams.set(
    "media_type",
    getMediaTypes(includeAudio, includeImages).join(",")
  );
  const results = await fetch(endpoint).then((res) => res.json());

  return results.collection.items.map((item) => ({
    id: item.data[0].nasa_id,
    asset: {
      preview: {
        src: item.links.find((link) => link.render === "image")?.href,
      },
    },
  }));
};
