# NASA Search Technical Challenge

## Getting Started

```
yarn install
yarn dev
```

Then go to localhost:3000 to see the app.

## Why I used these technologies

NextJS - quick to get up and running with routes and support for things like Tailwind and CSS Modules.

CSS Modules - straightforward way to add scoped styling to the application.

Fetch - Supported in all modern browsers, avoids downloading more dependencies.

CSS Grid & Flexbox - Widely supported in evergreen browsers now in 2021.

SWR - really useful React Hook for data fetching, removes the usual boilerplate around loading/error/data states.

Prettier - makes it easy to format things in a reasonable way

## What I would add with more time

### Support for Audio Files

I would add support for audio assets using the HTML5 <audio> tag.

### Testing around the UI components

I have setup Jest with JSDom and React Testing Library, however did not have time to write tests. If I did, I'd likely start with some high level "e2e" UI tests (i.e. search for an image, pick one in the grid and assert you see the asset details) as they deliver the most confidence and cover the "core flow".

### TypeScript

I wrote a few bits of code around type-checking input parameters that could be removed if I had used TypeScript. Because this app calls an API, if I wanted to make it resilient to unexpected API changes I would also add something like zod (https://github.com/colinhacks/zod) for runtime type checking. This would provide type validation for the API responses.

### Pagination or Lazy Loading

For terms like "moon" the NASA API returned 100 items in the first page, which led to a lot of images being displayed and downloaded at once. Lazy loading and infinite scroll could work well, and the SWR hook makes it easy to implement (https://swr.vercel.app/docs/pagination#useswrinfinite).

### Error Handling

The API performed well in development, but it could occasionally error out on a users maching (e.g. they lose signal, or the API server goes down). At the moment the app does not handle that gracefully, but SWR does retry requests in the background (https://swr.vercel.app/docs/error-handling). I would likely use a ErrorBoundry around the app to implement this, and render some sort of notification like UI informing the user.

### Loading States

The API was fast, however some of the images were very large and would download slowly on some connections. The API tells you the aspect ratio of the image in the metadata (which should be very quick to download), so you could use that to implement a skeleton loading UI.

### Explore SSR or SSG

NextJS has a lot of features that allow the server to do the heavy lifting. The details page could be entirely server side rendered, and left unhydrated on the client. This would also be heavily cacheable, which would further improve performance.

### Improve the UI

I think this app could look a lot better with some design enhancements. E.g. a faded background on the search page, maybe with the search off centre or on one corner of the screen for desktop users.
